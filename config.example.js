module.exports = {
    // IRC server to connect to.
    servers: {
        gimp: {
            url: 'irc.gimp.org',
            nick: 'gitlab-bot',
            password: 'thisisnotapassword',
            sasl: false
        },
        freenode: {
            url: 'irc.freenode.net',
            nick: 'gitlab-bot',
            password: 'thisisnotapassword',
            sasl: true
        }
    },

    // List of channels / people to report to.
    triggers: [
        {
            project: 'palvarez89/definitions',
            network: 'freenode',
            reports: {
                '##ironfoot': ['push', 'merge_request', 'issue'],
                'ironfoot': ['push', 'merge_request', 'issue']
            }
        },
        {
            project: 'palvarez89/morph',
            network: 'freenode',
            reports: {
                '##ironfoot2': ['push', 'merge_request', 'issue'],
                'ironfoot': ['push', 'merge_request', 'issue', 'build']
            }
        },
        {
            project: 'palvarez89/definitions',
            network: 'gimp',
            reports: {
                '##ironfoot': ['push', 'merge_request', 'issue'],
                'ironfoot': ['push', 'merge_request', 'issue', 'build']
            }
        },
    ],

    // IRC nick/names for the bot
    userName: 'gitlab-bot',
    realName: 'gitlab-bot',

    // Secret as entered in the Gitlab Webhook instance.
    secret: '12345',

    // Port on which to run.
    port: 1337,

    // Network interface on which to run the webhook server.
    hostname: '0.0.0.0',

    // Instance of lstu to shorten links -- keep empty to not use.
    lstu: 'https://lstu.fr',

    // Whether the irc client debug messages should be printed.
    debug: true
}
